const fs = require("fs")
const http = require("http")
const path = require("path")
const crypto = require("crypto")
const { promisify } = require("util")

const static_dir = __dirname + "/../common/static"
const sess_key = "ABCDEFG"
const sess_cookie_name = "sess"

const static = async (res, req_path) => {
	const filepath = path.join(static_dir, req_path)
	if (fs.existsSync(filepath)) {
		const stat = await fs.promises.stat(filepath)
		if (stat.isDirectory()) {
			return static(res, path.join(req_path, "/index.html"))
		} else if (stat.isFile()) {
			const mime = extToMime(path.extname(filepath))
			res.writeHead(200, {
				"Content-Type": mime,
				"Content-Length": stat.size,
			})
			fs.createReadStream(filepath).pipe(res)
			return true
		}
	}
	return false
}

const extToMime = (ext) => {
	return {
		".txt": "text/plain",
		".html": "text/html",
	}[ext]
}

const readCookie = (cookie) =>
	Object.fromEntries(
		cookie
			.split(";")
			.map((x) => x.trim().split("=").map(decodeURIComponent))
			.filter((e) => e.length === 2 && e[0] && e[1])
	)

const writeCookie = (res, name, value) => {
	const cookie = [[name, value].map(encodeURIComponent).join("="), "Path=/", "HttpOnly"].join(";")
	res.setHeader("Set-Cookie", cookie)
}

const sessEncrypt = async (str) => {
	const algorithm = "aes-192-cbc"
	const key = await promisify(crypto.scrypt)(sess_key, "salt", 24)
	const iv = Buffer.alloc(16, 0)

	const cipher = crypto.createCipheriv(algorithm, key, iv)

	return cipher.update(str, "utf8", "base64") + cipher.final("base64")
}

const sessDecrypt = async (cipher) => {
	const algorithm = "aes-192-cbc"
	const key = await promisify(crypto.scrypt)(sess_key, "salt", 24)
	const iv = Buffer.alloc(16, 0)

	const decipher = crypto.createDecipheriv(algorithm, key, iv)

	return decipher.update(cipher, "base64", "utf8") + decipher.final("utf8")
}

const readSession = async (cipher) => {
	if (!cipher) return
	const json = await sessDecrypt(cipher)
	if (!json) return
	return JSON.parse(json)
}

const writeSession = async (res, obj) => {
	const json = JSON.stringify(obj)
	const cipher = await sessEncrypt(json)
	writeCookie(res, sess_cookie_name, cipher)
}

const redirect = (res, to) => {
	res.writeHead(302, {
		Location: to,
	})
	res.end("")
}

const readBody = (req) => {
	return new Promise((r) => {
		let str = ""
		req.on("data", (chunk) => {
			str += chunk
		})
		req.on("end", () => {
			r(str)
		})
	})
}

const routes = [
	{
		// GET /page1
		match({ req, url }) {
			return req.method === "GET" && url.pathname === "/page1"
		},
		action({ req, res, sess }) {
			res.writeHead(200, { "Content-Type": "text/html" })
			res.end(require("../common/page1.js")({ now: new Date().toLocaleString() }))
		},
	},
	{
		// GET /secret/login
		match({ req, url }) {
			return req.method === "GET" && url.pathname === "/secret/login"
		},
		action({ req, res, sess }) {
			res.writeHead(200, { "Content-Type": "text/html" })
			res.end(require("../common/login.js")({}))
		},
	},
	{
		// POST /secret/login
		match({ req, url }) {
			return req.method === "POST" && url.pathname === "/secret/login"
		},
		async action({ req, res, url, sess }) {
			const params = new URLSearchParams(await readBody(req))
			const id = params.get("id")
			const pw = params.get("pw")
			const store = require("../common/store.js")
			if (store.authUser(id, pw)) {
				const to = url.searchParams.get("back") || "/secret/page2"
				await writeSession(res, { user: id })
				redirect(res, to)
			} else {
				res.writeHead(200, { "Content-Type": "text/html" })
				res.end(require("../common/login.js")({ error: "Wrong ID or PW." }))
			}
		},
	},
	{
		// POST /secret/logout
		match({ req, url }) {
			return req.method === "POST" && url.pathname === "/secret/logout"
		},
		async action({ req, res, sess }) {
			await writeSession(res, {})
			redirect(res, "/")
		},
	},
	{
		// GET /secret/page2
		match({ req, url }) {
			return req.method === "GET" && url.pathname === "/secret/page2"
		},
		action({ req, res, sess }) {
			res.writeHead(200, { "Content-Type": "text/html" })
			res.end(require("../common/page2.js")({ now: new Date().toLocaleString(), user: sess.user }))
		},
	},
]

const onRequest = async (req, res) => {
	const url = new URL(req.url, "http://" + req.headers.host)
	let sess = null

	// auth check
	if (url.pathname.startsWith("/secret/") && url.pathname !== "/secret/login") {
		const cookies = readCookie(req.headers.cookie || "")
		sess = await readSession(cookies[sess_cookie_name])
		if (!sess || !sess.user) {
			const back = url.href.slice(url.origin.length)
			redirect(res, "/secret/login?back=" + encodeURIComponent(back))
			return
		}
	}

	// static
	if (await static(res, url.pathname)) {
		return
	}

	// route
	for (const route of routes) {
		if (route.match({ req, url })) {
			await route.action({ req, res, url, sess })
			return
		}
	}

	// route not found
	res.writeHead(404)
	res.end("Page not found")
}

const serve = () => {
	http.createServer((req, res) => {
		onRequest(req, res).catch((err) => {
			console.error(err)
			res.writeHead(500)
			res.end("Server error")
		})
	}).listen(8000)
}

serve()
