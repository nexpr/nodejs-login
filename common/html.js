const htmlEscape = (str) => {
	return str.replace(/[&<>"]/g, (c) => {
		switch (c) {
			case "&": return "&amp;"
			case "<": return "&lt;"
			case ">": return "&gt;"
			case "\x22": return "&quot;"
		}
	})
}

const htmlize = (value) => {
	if (Array.isArray(value)) return value.map(htmlize).join("")
	if (value.html) return value.html
	if (value.text) return htmlEscape(value.text)
	return htmlEscape(String(value))
}

module.exports = (tpls, ...values) => {
	return tpls.reduce((a, b, i) => a + htmlize(values[i - 1]) + b)
}
