const html = require("./html.js")
module.exports = (values) => html`
	<h1>Login</h1>
	${values.error ? { html: html`<p><b>${values.error}</b></p>` } : ""}
	<form method="POST">
		<label>
			ID
			<input name="id" />
		</label>
		<label>
			Password
			<input name="pw" type="password" />
		</label>
		<button>POST</button>
	</form>
`
