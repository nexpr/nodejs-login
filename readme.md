# Node.js framework ログイン比較

## frameworks

- nodejs
- koa
- hapi
- fastify

## application

### pages

```
/                      index.html 静的ファイル
/static1.txt           静的ファイル
/page1                 時刻を表示する動的ページ
/secret/login          ログインページ / POST するとログイン処理
/secret/logout         POST するとログアウト処理して / へリダイレクト
/secret/static2.txt    静的ファイル （ログイン必要）
/secret/page2          ユーザ名などを表示する動的ページ （ログイン必要）
```

### pattern

- 1.js:
    - 未ログイン状態のアクセスは `/secret/login` （ログインページ）にリダイレクト
    - クエリパラメータ back で戻り先保持
- 2.js:
    - 未ログイン状態のアクセスはアクセスしたページがログインページになる

