const Koa = require("koa")
const static = require("koa-static")
const session = require("koa-session")
const Router = require("@koa/router")
const bodyParser = require("koa-bodyparser")

const static_dir = __dirname + "/../common/static"

const app = new Koa()
app.keys = ["ABCDEFG"]

app.use(session(app))

const login = (ctx) => {
	const { id, pw } = ctx.request.body
	const store = require("../common/store.js")
	if (store.authUser(id, pw)) {
		ctx.session = { user: id }
		ctx.redirect(ctx.url)
	} else {
		ctx.body = require("../common/login.js")({ error: "Wrong ID or PW." })
	}
}

app.use((ctx, next) => {
	if (ctx.path.startsWith("/secret/")) {
		if (!ctx.session || !ctx.session.user) {
			if (ctx.method === "POST") {
				return bodyParser()(ctx, login.bind(null, ctx))
			} else {
				ctx.body = require("../common/login.js")({})
			}
			return
		}
	}
	return next()
})

app.use(static(static_dir))

const router = new Router()

router.get("/page1", (ctx) => {
	ctx.body = require("../common/page1.js")({ now: new Date().toLocaleString() })
})

router.post("/secret/logout", (ctx) => {
	ctx.session = {}
	ctx.redirect("/")
})

router.get("/secret/page2", (ctx) => {
	ctx.body = require("../common/page2.js")({ now: new Date().toLocaleString(), user: ctx.session.user })
})

app.use(router.routes())

app.listen(8000)
