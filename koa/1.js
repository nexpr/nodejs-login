const Koa = require("koa")
const static = require("koa-static")
const session = require("koa-session")
const Router = require("@koa/router")
const bodyParser = require("koa-bodyparser")

const static_dir = __dirname + "/../common/static"

const app = new Koa()
app.keys = ["ABCDEFG"]

app.use(session(app))

app.use((ctx, next) => {
	if (ctx.path.startsWith("/secret/") && ctx.path !== "/secret/login") {
		if (!ctx.session || !ctx.session.user) {
			ctx.redirect("/secret/login?back=" + encodeURIComponent(ctx.url))
			return
		}
	}
	return next()
})

app.use(static(static_dir))

const router = new Router()

router.get("/page1", (ctx) => {
	ctx.body = require("../common/page1.js")({ now: new Date().toLocaleString() })
})

router.get("/secret/login", (ctx) => {
	ctx.body = require("../common/login.js")({})
})

router.post("/secret/login", bodyParser(), (ctx) => {
	const { id, pw } = ctx.request.body
	const store = require("../common/store.js")
	if (store.authUser(id, pw)) {
		ctx.session = { user: id }
		const to = ctx.query.back || "/secret/page2"
		ctx.redirect(to)
	} else {
		ctx.body = require("../common/login.js")({ error: "Wrong ID or PW." })
	}
})

router.post("/secret/logout", (ctx) => {
	ctx.session = {}
	ctx.redirect("/")
})

router.get("/secret/page2", (ctx) => {
	ctx.body = require("../common/page2.js")({ now: new Date().toLocaleString(), user: ctx.session.user })
})

app.use(router.routes())

app.listen(8000)
