const Hapi = require("@hapi/hapi")

!(async function () {
	const server = Hapi.server({
		port: 8000,
		routes: {
			files: {
				relativeTo: __dirname + "/../common/static",
			},
		},
	})

	await server.register(require("@hapi/inert"))
	await server.register(require("@hapi/cookie"))

	server.auth.strategy("session", "cookie", {
		cookie: {
			path: "/",
			password: "ABCD1234".repeat(4),
			isSecure: false,
		},

		redirectTo: "/secret/login",
	})

	server.route([
		{
			method: "GET",
			path: "/page1",
			handler(req, h) {
				return require("../common/page1.js")({ now: new Date().toLocaleString() })
			},
		},
		{
			method: "GET",
			path: "/secret/login",
			handler(req, h) {
				return require("../common/login.js")({})
			},
		},
		{
			method: "POST",
			path: "/secret/login",
			handler(req, h) {
				const { id, pw } = req.payload
				const store = require("../common/store.js")
				if (store.authUser(id, pw)) {
					req.cookieAuth.set({ user: id })
					const to = req.query.back || "/secret/page2"
					return h.redirect(to)
				} else {
					return require("../common/login.js")({ error: "Wrong ID or PW." })
				}
			},
		},
		{
			method: "POST",
			path: "/secret/logout",
			options: {
				auth: "session",
			},
			handler(req, h) {
				req.cookieAuth.clear()
				return h.redirect("/")
			},
		},
		{
			method: "GET",
			path: "/secret/page2",
			options: {
				auth: "session",
			},
			handler(req) {
				return require("../common/page2.js")({
					now: new Date().toLocaleString(),
					user: req.auth.credentials.user,
				})
			},
		},
		{
			method: "GET",
			path: "/secret/{path*}",
			options: {
				auth: "session",
			},
			handler: {
				directory: {
					path: "secret",
				},
			},
		},
		{
			method: "GET",
			path: "/{path*}",
			handler: {
				directory: {
					path: ".",
				},
			},
		},
	])

	await server.start()
})()
