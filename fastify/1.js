const fastify = require("fastify")()

const cookie_name = "sess"
const cookie_option = {
	path: "/",
	httpOnly: true,
	signed: true,
}

!(async function () {
	await fastify.register(require("fastify-formbody"))

	await fastify.register(require("fastify-cookie"), {
		secret: "ABCDEFG",
	})

	await fastify.register(require("fastify-static"), {
		root: __dirname + "/../common/static",
	})

	fastify.addHook("onRequest", async (request, reply) => {
		request.parsed_url = new URL("http://localhost" + request.url)
		const path = request.parsed_url.pathname

		if (path.startsWith("/secret/") && path !== "/secret/login") {
			const sess = JSON.parse(reply.unsignCookie(request.cookies[cookie_name] || "") || "null")
			request.sess = sess
			if (!sess || !sess.user) {
				reply.redirect("/secret/login?back=" + encodeURIComponent(request.url))
			}
		}
	})

	fastify.get("/page1", (request, reply) => {
		reply.type("text/html").send(require("../common/page1.js")({ now: new Date().toLocaleString() }))
	})

	fastify.get("/secret/login", (requst, reply) => {
		reply.type("text/html").send(require("../common/login.js")({}))
	})

	fastify.post("/secret/login", (request, reply) => {
		const { id, pw } = request.body
		const store = require("../common/store.js")
		if (store.authUser(id, pw)) {
			const to = request.query.back || "/secret/page2"
			reply.setCookie(cookie_name, JSON.stringify({ user: id }), cookie_option).redirect(to)
		} else {
			reply.type("text/html").send(require("../common/login.js")({ error: "Wrong ID or PW." }))
		}
	})

	fastify.post("/secret/logout", (request, reply) => {
		reply.clearCookie(cookie_name).redirect("/")
	})

	fastify.get("/secret/page2", (request, reply) => {
		reply
			.type("text/html")
			.send(require("../common/page2.js")({ now: new Date().toLocaleString(), user: request.sess.user }))
	})

	await fastify.listen(8000)
})()
