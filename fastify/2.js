const fastify = require("fastify")()

const cookie_name = "sess"
const cookie_option = {
	path: "/",
	httpOnly: true,
	signed: true,
}

!(async function () {
	await fastify.register(require("fastify-formbody"))

	await fastify.register(require("fastify-cookie"), {
		secret: "ABCDEFG",
	})

	await fastify.register(require("fastify-static"), {
		root: __dirname + "/../common/static",
	})

	fastify.addHook("onRequest", async (request, reply) => {
		request.parsed_url = new URL("http://localhost" + request.url)
		const path = request.parsed_url.pathname

		if (path.startsWith("/secret/")) {
			const sess = JSON.parse(reply.unsignCookie(request.cookies[cookie_name] || "") || "null")
			request.sess = sess
			if (!sess || !sess.user) {
				if (request.method === "POST") {
					request.should_auth = true
				} else {
					reply.type("text/html").send(require("../common/login.js")({}))
				}
			}
		}
	})

	fastify.addHook("preValidation", async (request, reply) => {
		if (!request.should_auth) return

		const { id, pw } = request.body
		const store = require("../common/store.js")
		if (store.authUser(id, pw)) {
			reply.setCookie(cookie_name, JSON.stringify({ user: id }), cookie_option).redirect(request.url)
		} else {
			reply.type("text/html").send(require("../common/login.js")({ error: "Wrong ID or PW." }))
		}
	})

	fastify.get("/page1", (request, reply) => {
		reply.type("text/html").send(require("../common/page1.js")({ now: new Date().toLocaleString() }))
	})

	fastify.post("/secret/logout", (request, reply) => {
		reply.clearCookie(cookie_name).redirect("/")
	})

	fastify.get("/secret/page2", (request, reply) => {
		reply
			.type("text/html")
			.send(require("../common/page2.js")({ now: new Date().toLocaleString(), user: request.sess.user }))
	})

	await fastify.listen(8000)
})()
